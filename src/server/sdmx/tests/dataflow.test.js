import Parser, { makeValues } from '../dataflow'
import emptyConstraint from '../../tests/mocks/sdmx/empty_constraint'
import AIR_EMISSIONS_DF from '../../tests/mocks/sdmx/dataflow_AIR_EMISSIONS_DF'
import { or } from 'ramda'

const mockedDate = new Date(2017, 11, 10)
global.Date = jest.fn(() => mockedDate)
global.Date.now = global.Date

const defaultParserOptions = { datasourceId: 'TEST', dimensionValuesLimit: 1000 }

describe('SDMX | dataflow', () => {
  it('should make dataflows (abs)', () => {
    const parser = Parser(require('../../tests/mocks/sdmx/dataflow')(), defaultParserOptions)
    const dataflows = parser.dataflows()
    expect(dataflows[0].fields.dataflowId).toEqual('DF_TOURISM_ENT_EMP')
    expect(dataflows[0].fields.agencyId).toEqual('OECD.CFE')
    expect(dataflows[0].fields.datasourceId).toEqual('TEST')
    expect(dataflows[0].dimensions.length).toEqual(5)
    expect(dataflows[0].categories.length).toEqual(1)
    expect(dataflows[0].fields.name.en).toEqual('Enterprises and employment in tourism')
    expect(dataflows[0].fields.name.fr).toEqual('Enterprises and employment in tourism (fr)')
    expect(dataflows[0].fields.description.en).toEqual('Description of Enterprises and employment in tourism')
    expect(dataflows[0].fields.description.fr).toEqual('Description of Enterprises and employment in tourism (fr)')
    expect(dataflows[0].dimensions[0].name.en).toEqual('Reference area')
    expect(dataflows[0].dimensions[0].name.fr).toEqual('Reference area (fr)')
    // expect(dataflows[0].dimensions[0].children[0].name.en).toEqual('Argentina');
    // expect(dataflows[0].dimensions[0].children[0].name.fr).toEqual('Total (fr)');
    expect(dataflows[0].hasNoContraints).toBeFalsy()
    expect(JSON.stringify(dataflows)).toMatchSnapshot()
  })

  it('should make dataflows (without content constraints)', () => {
    const parser = Parser(require('../../tests/mocks/sdmx/dataflow_without_cc')(), defaultParserOptions)
    const dataflows = parser.dataflows()
    expect(JSON.stringify(dataflows)).toMatchSnapshot()
    expect(dataflows[0].hasNoContraints).toBeTruthy()
  })

  it('should make dataflows with data availability', () => {
    const parser = Parser(AIR_EMISSIONS_DF, defaultParserOptions)
    const dataflows = parser.dataflows()
    expect(JSON.stringify(dataflows)).toMatchSnapshot()
    expect(dataflows[0].fields.id).toEqual('TEST:AIR_EMISSIONS_DF')
    expect(dataflows[0].fields.agencyId).toEqual('OECD')
    expect(dataflows[0].fields.datasourceId).toEqual('TEST')
    expect(dataflows[0].dimensions.length).toEqual(2) // 2 of 3
    expect(dataflows[0].dimensions[0].children.length).toEqual(39) // nodes without data are kept for paths
    expect(dataflows[0].hasNoContraints).toBeFalsy()
  })

  it('should limit facets with dimensionValuesLimit', () => {
    const parser = Parser(AIR_EMISSIONS_DF, { datasourceId: 'TEST', dimensionValuesLimit: 5 })
    const dataflows = parser.dataflows()
    expect(JSON.stringify(dataflows)).toMatchSnapshot()
    expect(dataflows[0].fields.id).toEqual('TEST:AIR_EMISSIONS_DF')
    expect(dataflows[0].fields.agencyId).toEqual('OECD')
    expect(dataflows[0].fields.datasourceId).toEqual('TEST')
    expect(dataflows[0].dimensions.length).toEqual(1) // -1 CC, -1 limit, 1 of 3
  })

  it('should have empty constraints', () => {
    const parser = Parser(emptyConstraint, defaultParserOptions)
    const dataflows = parser.dataflows()
    expect(JSON.stringify(dataflows)).toMatchSnapshot()
    expect(dataflows[0].hasEmptyConstraints).toBeTruthy()
    expect(dataflows[0].hasNoContraints).toBeFalsy()
  })

  it('should make values', () => {
    const values = [
      { id: 1, names: { en: '1' }, parentId: undefined, hasData: true },
      { id: 11, names: { en: '11' }, parentId: 1, hasData: false },
      { id: 12, names: { en: '12' }, parentId: 1, hasData: true },
      { id: 121, names: { en: '121' }, parentId: 12, hasData: false },
      { id: 122, names: { en: '122' }, parentId: 12, hasData: true },
      { id: 1221, names: { en: '1221' }, parentId: 122, hasData: false },
      { id: 1222, names: { en: '1222' }, parentId: 122, hasData: true },
    ]
    const tree = ({ hasNoContraints }) => [
      {
        id: 1,
        name: { en: '1' },
        parent: undefined,
        hasData: or(true, hasNoContraints),
        children: [
          { id: 11, name: { en: '11' }, parent: 1, hasData: or(false, hasNoContraints), children: [] },
          {
            id: 12,
            name: { en: '12' },
            parent: 1,
            hasData: or(true, hasNoContraints),
            children: [
              { id: 121, name: { en: '121' }, parent: 12, hasData: or(false, hasNoContraints), children: [] },
              {
                id: 122,
                name: { en: '122' },
                parent: 12,
                hasData: or(true, hasNoContraints),
                children: [
                  { id: 1221, name: { en: '1221' }, parent: 122, hasData: or(false, hasNoContraints), children: [] },
                  { id: 1222, name: { en: '1222' }, parent: 122, hasData: or(true, hasNoContraints), children: [] },
                ],
              },
            ],
          },
        ],
      },
    ]
    expect(makeValues({ hasNoContraints: false })(values)).toEqual(tree({ hasNoContraints: false }))
    expect(makeValues({ hasNoContraints: true })(values)).toEqual(tree({ hasNoContraints: true }))
  })
})
