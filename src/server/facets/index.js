import { map, reduce, pathOr } from 'ramda';
import { doSearch } from '../search';

export const getCache = (solr, config) => {
  const localizedSearches = map(
    lang => doSearch(null, '', null, lang, solr, config).then(({ facets }) => [lang, facets]),
    pathOr([], ['dataflows', 'locales'], config),
  );
  return Promise.all(localizedSearches).then(reduce((acc, [lang, facets]) => ({ ...acc, [lang]: facets }), {}));
};
