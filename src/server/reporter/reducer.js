import { toPairs, reduce, reject } from 'ramda'

export const createRootReducer = (initalState, reducer) => {
  let state = initalState
  const root = reducer
  let listeners = []

  const unregister = cb => {
    listeners = reject(x => x === cb, listeners)
  }
  return {
    getState: () => state,
    listen: cb => {
      listeners.push(cb)
      return () => unregister(cb)
    },
    dispatch: async action => {
      state = reduce((acc, [key, fn]) => ({ ...acc, [key]: fn(acc[key], action) }), state, toPairs(root))
      listeners.forEach(cb => cb(state, action))
      return action
    },
  }
}
