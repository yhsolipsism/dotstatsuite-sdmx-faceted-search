export const START = 'START';
const reducer = (state, action) => {
  switch (action.type) {
    case START:
      return { ...state, startTime: new Date() };
    default:
      return state;
  }
};

export default reducer;
