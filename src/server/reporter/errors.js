import { pick } from 'ramda'
import debug from '../debug'

export const ERROR = 'ADD_ERROR'
export const WARNING = 'WARNING'

const reducer = (state, action) => {
  try {
    switch (action.type) {
      case ERROR:
        return [{ ...pick(['code', 'message'], action.error), time: new Date() }, ...state]
      default:
        return state
    }
  } catch (e) {
    debug.error(e)
  }
}

export default reducer
