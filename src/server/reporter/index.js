import { createRootReducer } from './reducer';
import process from './process';
import loadings from './loadings';

const rootReducer = {
  process,
  loadings,
};

const initialState = {
  process: {},
  loadings: {},
};

const report = createRootReducer(initialState, rootReducer);
const Report = () => {
  const fct = action => (!action ? report.getState() : report.dispatch(action));
  fct.listen = report.listen;
  return fct;
};
export default Report;
