export default {
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    contentLanguages: ['en'],
    id: 'IDREF1',
    prepared: '2019-03-01T09:33:28.0392924Z',
    test: false,
  },
  data: {
    dataflows: [
      {
        id: 'DF_ILMS_ALL_MFL_TEMP_OCU_NB',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ILO:DF_ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
            type: 'dataflow',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
            type: 'datastructure',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Inflow of employed migrants by occupation' },
        description: {
          en:
            'Inflow of employed migrants refer to the number of persons who changed their country of usual residence and were also employed during a specified brief period. Data are disaggregated by occupation according to the latest version of the International Standard Classification of Occupations (ISCO-08). Information on occupation provides a description of the set of tasks and duties which are carried out by, or can be assigned to, one person.',
        },
        annotations: [
          { type: 'LAST_UPDATE', text: { en: '3/5/2018 3:52:29 AM' } },
          { type: 'LAYOUT_COLUMN', text: { en: 'TIME_PERIOD' } },
          { type: 'LAYOUT_ROW', text: { en: 'REF_AREA,SURVEY' } },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
      },
    ],
    agencySchemes: [
      {
        id: 'AGENCIES',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.base.AgencyScheme=SDMX:AGENCIES(1.0)',
            type: 'agencyscheme',
          },
        ],
        version: '1.0',
        agencyID: 'SDMX',
        isExternalReference: false,
        isFinal: false,
        name: { en: 'SDMX Agency Scheme' },
        isPartial: false,
        agencies: [
          {
            id: 'SDMX',
            name: { en: 'SDMX' },
            links: [{ rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.base.Agency=SDMX', type: 'agency' }],
          },
          {
            id: 'ILO',
            name: { en: 'International Labour Organization (ILO)' },
            links: [{ rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.base.Agency=ILO', type: 'agency' }],
          },
        ],
      },
    ],
    categorySchemes: [
      {
        id: 'CAS_SUBJECT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=ILO:CAS_SUBJECT(1.0)',
            type: 'categoryscheme',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Browse Indicators by Subject' },
        isPartial: false,
        categories: [
          {
            id: 'SDG',
            name: { en: 'SDG labour market indicators' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).SDG',
                type: 'category',
              },
            ],
          },
          {
            id: 'ILOEST',
            name: { en: 'ILO modelled estimates' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).ILOEST',
                type: 'category',
              },
            ],
          },
          {
            id: 'EAP',
            name: { en: 'Population and labour force' },
            description: {
              en:
                '<p>The <em>total population</em> comprises persons of all ages who were living in the country during the reference period, regardless of residency status or citizenship.</p> <p>For statistical purposes, the <em>working age population</em> comprises all persons above a specified minimum age threshold for which an inquiry on economic activity is made. For purposes of international comparability, the working age population is commonly defined as persons aged 15 years and older, but this varies from country to country. In addition to using a minimum age threshold, certain countries also apply a maximum age limit. Adoption of a specified upper age limit means that all persons above that age limit are excluded from the count of the working age population. Most countries, however, do not use a maximum age limit.</p> <p>Labour force comprises all persons of working age who furnish the supply of labour for the production of goods and services (as defined by the United Nations System of National Accounts (SNA) production boundary) during a specified time-reference period. It refers to the sum of all persons of working age who are employed and those who are unemployed.</p> <p>The <em>labour force participation rate </em>is calculated as the labour force during a given reference period given as a percent of the working age population in the same reference period.</p> <p>For more detailed information, please refer to the <a href="http://www.ilo.org/global/statistics-and-databases/standards-and-guidelines/resolutions-adopted-by-international-conferences-of-labour-statisticians/WCMS_087481/lang--en/index.htm"><em>Resolution concerning statistics of the economically active population, employment, unemployment and underemployment</em></a>, adopted by the Thirteenth International Conference of Labour Statisticians (October 1982).</p> <p>&nbsp;</p>',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).EAP',
                type: 'category',
              },
            ],
          },
          {
            id: 'EMP',
            name: { en: 'Employment' },
            description: {
              en:
                '<p> The <em>employed</em> comprise all persons of working age who during a specified brief period, such as one week or one day, were in the following categories: a) paid employment (whether at work or with a job but not at work); or b) self-employment (whether at work or with an enterprise but not at work).</p> <p> The <em>employment-to-population ratio (EPR)</em> is calculated as the number of persons who are employed during a given reference period as a percent of the total of working age population in the same reference period.</p>',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).EMP',
                type: 'category',
              },
            ],
          },
          {
            id: 'LUU',
            name: { en: 'Unemployment and labour underutilization' },
            description: {
              en:
                '<p>The <em>unemployed</em> comprise all persons of working age who were: a) without work during the reference period, i.e. were not in paid employment or self-employment; b) currently available for work, i.e. were available for paid employment or self-employment during the reference period; and c) seeking work, i.e. had taken specific steps in a specified recent period to seek paid employment or self-employment. For purposes of international comparability, the period of job search is often defined as the preceding four weeks, but this varies from country to country.</p> <p>The specific steps taken to seek employment may include registration at a public or private employment exchange; application to employers; checking at worksites, farms, factory gates, market or other assembly places; placing or answering newspaper advertisements; seeking assistance of friends or relatives; looking for land, building, machinery or equipment to establish own enterprise; arranging for financial resources; and applying for permits and licences.</p> <p>The <em>unemployment rate</em>&nbsp;is calculated as the number of persons who are unemployed during the reference period given as a percent of the total number of employed and unemployed persons (i.e., the labour force) in the same reference period.</p> <p>Persons outside the labour force comprise all persons of working age who, during the specified reference period, were not in the labour force (that is, were not employed or unemployed). The working age population is commonly defined as persons aged 15 years and older, but this varies from country to country. In addition to using a minimum age threshold, certain countries also apply a maximum age limit.</p> <p>The <em>inactivity rate</em> conveys the number of persons of working age not economically active (persons outside the labour force) expressed as a percentage of the working age population.</p> <p>The discouraged job-seekers are a subset of the persons outside the labour force. They are those persons of working age who during a specified reference period were without work and available for work, but did not look for work in the recent past for specific reasons (for example, believing that there were no jobs available, there were none for which they would qualify, or having given up hope of finding employment).</p>',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).LUU',
                type: 'category',
              },
            ],
          },
          {
            id: 'HOW',
            name: { en: 'Working time' },
            description: {
              en:
                '<p> The concept of <em>hours actually worked</em> within the SNA production boundary relates to the time that persons in employment spend directly on, and in relation to, productive activities; down time; and resting time during a specified time reference period. It thus <strong>includes</strong> (a) &ldquo;direct hours&rdquo; or the time spent carrying out the tasks and duties of a job, (b) &ldquo;related hours&rdquo;, or the time spent maintaining, facilitating or enhancing productive activities (c) &ldquo;down time&rdquo;, or time when a person in a job cannot work due to machinery or process breakdown, accident, lack of supplies or power or Internet access and (d) &ldquo;resting time&rdquo;, or time spent in short periods of rest, relief or refreshment, including tea, coffee or prayer breaks, generally practised by custom or contract according to established norms and/or national circumstances.</p> <p> <em>Hours actually worked</em> <strong>excludes</strong> time not worked during activities such as: (a) Annual leave, public holidays, sick leave, parental leave or maternity/paternity leave, other leave for personal or family reasons or civic duty, (b) Commuting time between work and home when no productive activity for the job is performed; for paid employment, even when paid by the employer; (c) Time spent in certain educational activities; for paid employment, even when authorized, paid or provided by the employer; (d) Longer breaks distinguished from short resting time when no productive activity is performed (such as meal breaks or natural repose during long trips); for paid employment, even when paid by the employer.</p> <p> For a paid-employment job, hours paid for refers to the time for which employees have received payment from their employer (at normal or premium rates, in cash or in kind) during a specified reference period, regardless of whether the hours were actually worked or not. It <strong>includes</strong> time paid but not worked such as paid annual leave, paid public holidays and certain absences such as paid sick leave, and <strong>excludes</strong> time worked but not paid by the employer, such as unpaid overtime, and absences that are not paid by the employer, such as unpaid educational leave or maternity leave.</p> <p> For more detailed information, please refer to the <em><a href="http://www.ilo.org/global/statistics-and-databases/standards-and-guidelines/resolutions-adopted-by-international-conferences-of-labour-statisticians/WCMS_112455/lang--en/index.htm">Resolution concerning the measurement of working time</a>, </em>adopted by the Eighteenth International Conference of Labour Statisticians (December 2008).</p>',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).HOW',
                type: 'category',
              },
            ],
          },
          {
            id: 'EAR',
            name: { en: 'Earnings and labour cost' },
            description: {
              en:
                '<p>The concept of <em>earnings</em>, as applied in wages statistics, relates to gross remuneration in cash and in kind paid to employees, as a rule at regular intervals, for time worked or work done together with remuneration for time not worked, such as annual vacation, other type of paid leave or holidays. Earnings exclude employers&rsquo; contributions in respect of their employees paid to social security and pension schemes and also the benefits received by employees under these schemes. Earnings also exclude severance and termination pay.</p> <p>For more detailed information, please refer to the <a href="http://www.ilo.org/global/statistics-and-databases/standards-and-guidelines/resolutions-adopted-by-international-conferences-of-labour-statisticians/WCMS_087496/lang--en/index.htm"><em>Resolution concerning an integrated system of wages statistics</em>,</a> adopted by the Twelfth International Conference of Labour Statisticians (October 1973).</p> <p><em>Employment-related income</em> of self-employed workers consists of the payments, in cash, in kind or in services, which are received by individuals, for themselves or in respect of their family members, as a result of their current or former involvement in self-employment jobs during a specified reference period. Statistics of employment-related income should relate to the gross remuneration of self-employed workers. This concept includes: a) the profit (or the share of profit) which is generated by the self-employment activity; b) where relevant, the remuneration received by owner-managers of corporations and quasi-corporations; and c) the amount of employment-related social security benefits received by self-employed persons through schemes recognizing the status in employment as a specific condition for membership. It excludes income derived from other sources such as property, social assistance, transfers, etc., not related to employment.</p> <p>For more detailed information, please refer to the <a href="http://www.ilo.org/global/statistics-and-databases/standards-and-guidelines/resolutions-adopted-by-international-conferences-of-labour-statisticians/WCMS_087490/lang--en/index.htm"><em>Resolution concerning the measurement of employment-related income</em></a>, adopted by the Sixteenth International Conference of Labour Statisticians (October 1998).</p>',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).EAR',
                type: 'category',
              },
            ],
          },
          {
            id: 'LPY',
            name: { en: 'Labour productivity' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).LPY',
                type: 'category',
              },
            ],
          },
          {
            id: 'SOC',
            name: { en: 'Social protection' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).SOC',
                type: 'category',
              },
            ],
          },
          {
            id: 'OSH',
            name: { en: 'Safety and health at work' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).OSH',
                type: 'category',
              },
            ],
          },
          {
            id: 'IR',
            name: { en: 'Industrial relations' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).IR',
                type: 'category',
              },
            ],
          },
          {
            id: 'MIG',
            name: { en: 'Labour migration' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).MIG',
                type: 'category',
              },
            ],
          },
          {
            id: 'CPI',
            name: { en: 'Consumer prices' },
            description: {
              en:
                '<p>The <em>Consumer Price Index (CPI)</em> is a current social and economic indicator that is constructed to measure changes over time in the general level of prices of consumer goods and services that households acquire, use or pay for consumption.</p> <p>The index aims to measure the change in consumer prices over time. This may be done by measuring the cost of purchasing a fixed basket of consumer goods and services of constant quality and similar characteristics, with the products in the basket being selected to be representative of households&rsquo; expenditure during a year or other specified period. Such an index is called a fixed-basket price index.</p> <p>The index may also aim to measure the effects of price changes on the cost of achieving a constant standard of living (i.e. level of utility or welfare). This concept is called a cost-of-living index (COLI). A fixed basket price index, or another appropriate design, may be employed as an approximation to a COLI.</p> <p>A consumer price index is usually estimated as a series of summary measures of the period-to-period proportional change in the prices of a fixed set of consumer goods and services of constant quantity and characteristics, acquired, used or paid for by the reference population. Each summary measure is constructed as a weighted average of a large number of elementary aggregate indices. Each of the elementary aggregate indices is estimated using a sample of prices for a defined set of goods and services obtained in, or by residents of, a specific region from a given set of outlets or other sources of consumption goods and services.</p> <p>For more detailed information, please refer to the <a href="http://www.ilo.org/global/statistics-and-databases/standards-and-guidelines/resolutions-adopted-by-international-conferences-of-labour-statisticians/WCMS_087521/lang--en/index.htm"><em>Resolution concerning consumer price indices</em></a>, adopted by the Seventeenth International Conference of Labour Statisticians (Geneva, 2003).</p>',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).CPI',
                type: 'category',
              },
            ],
          },
        ],
      },
    ],
    categorisations: [
      {
        id: 'CAT_MIG_DF_ILMS_ALL_MFL_TEMP_OCU_NB',
        links: [
          {
            rel: 'self',
            urn:
              'urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=ILO:CAT_MIG_DF_ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
            type: 'categorisation',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: "Category 'ILO.CAS_SUBJECT.MIG' linking to Dataflow 'DF_ILMS_ALL_MFL_TEMP_OCU_NB'" },
        source: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ILO:DF_ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
        target: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ILO:CAS_SUBJECT(1.0).MIG',
      },
    ],
    conceptSchemes: [
      {
        id: 'CS_CLASSIF_TYPE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ILO:CS_CLASSIF_TYPE(1.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Classifications' },
        isPartial: true,
        concepts: [
          {
            id: 'OCU',
            name: { en: 'Occupation' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_CLASSIF_TYPE(1.0).OCU',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_OCU(1.0)' },
          },
        ],
      },
      {
        id: 'CS_ILOSTAT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ILO:CS_ILOSTAT(1.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'ILOSTAT Concepts' },
        isPartial: false,
        concepts: [
          {
            id: 'COLLECTION',
            name: { en: 'Data collection' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).COLLECTION',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_COLLECTION(1.0)' },
          },
          {
            id: 'REF_AREA',
            name: { en: 'Reference area' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).REF_AREA',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_AREA(1.0)' },
          },
          {
            id: 'FREE_TEXT_NOTE',
            name: {
              en:
                'Free text note; should only be used when a note cannot be coded or in the case of multiple values for one note type',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).FREE_TEXT_NOTE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'FREQ',
            name: { en: 'Frequency' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).FREQ',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_FREQ(1.0)' },
          },
          {
            id: 'OBS_VALUE',
            name: { en: 'Observation value' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).OBS_VALUE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'TIME_PERIOD',
            name: { en: 'Time period' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).TIME_PERIOD',
                type: 'concept',
              },
            ],
          },
          {
            id: 'DECIMALS',
            name: { en: 'Decimals' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).DECIMALS',
                type: 'concept',
              },
            ],
          },
          {
            id: 'SURVEY',
            name: { en: 'Survey' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).SURVEY',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_SURVEY(1.0)' },
          },
          {
            id: 'OBS_STATUS',
            name: { en: 'Observation status' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).OBS_STATUS',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_OBS_STATUS(1.0)' },
          },
          {
            id: 'UNIT_MEASURE_TYPE',
            name: { en: 'Unit of measure type' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).UNIT_MEASURE_TYPE',
                type: 'concept',
              },
            ],
            coreRepresentation: {
              enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MEASURE_TYPE(1.0)',
            },
          },
          {
            id: 'UNIT_MEASURE',
            name: { en: 'Unit of measure' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).UNIT_MEASURE',
                type: 'concept',
              },
            ],
            coreRepresentation: {
              enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MEASURE(1.0)',
            },
          },
          {
            id: 'UNIT_MULT',
            name: { en: 'Unit multiplier' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).UNIT_MULT',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MULT(1.0)' },
          },
          {
            id: 'REPRESENTED_VARIABLE',
            name: { en: 'Represented Variable' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).REPRESENTED_VARIABLE',
                type: 'concept',
              },
            ],
            coreRepresentation: {
              enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_REPRESENTED_VARIABLE(1.0)',
            },
          },
          {
            id: 'INDICATOR',
            name: { en: 'Indicator' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).INDICATOR',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_INDICATOR(1.0)' },
          },
          {
            id: 'SOURCE',
            name: { en: 'Source' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).SOURCE',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_SOURCE(1.0)' },
          },
        ],
      },
      {
        id: 'CS_NOTE_TYPE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ILO:CS_NOTE_TYPE(1.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Note Types' },
        isPartial: true,
        concepts: [
          {
            id: 'S3',
            name: { en: 'Data reference period' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).S3',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_S3(1.0)' },
          },
          {
            id: 'C6',
            name: { en: 'Nonstandard age group' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).C6',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_C6(1.0)' },
          },
          {
            id: 'S9',
            name: { en: 'Reference group coverage' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).S9',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_S9(1.0)' },
          },
          {
            id: 'I11',
            name: { en: 'Break in series' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).I11',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_I11(1.0)' },
          },
          {
            id: 'T2',
            name: { en: 'Age coverage - minimum age' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).T2',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_T2(1.0)' },
          },
          {
            id: 'T36',
            name: { en: 'Definition of migrants' },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).T36',
                type: 'concept',
              },
            ],
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_T36(1.0)' },
          },
        ],
      },
    ],
    codelists: [
      {
        id: 'CL_AREA',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_AREA(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Reference Area' },
        isPartial: true,
        codes: [
          {
            id: 'ARE',
            name: { en: 'United Arab Emirates' },
            annotations: [
              { type: 'ORDER', text: { en: '11175' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'AE' } },
              { type: 'M49Code', text: { en: '784' } },
              { type: 'ILO_REGION', text: { en: 'X36' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X05' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).ARE', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'FJI',
            name: { en: 'Fiji' },
            annotations: [
              { type: 'ORDER', text: { en: '10350' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'FJ' } },
              { type: 'M49Code', text: { en: '242' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X04' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).FJI', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'KGZ',
            name: { en: 'Kyrgyzstan' },
            annotations: [
              { type: 'ORDER', text: { en: '10590' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'KG' } },
              { type: 'M49Code', text: { en: '417' } },
              { type: 'ILO_REGION', text: { en: 'X60' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X03' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).KGZ', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'KHM',
            name: { en: 'Cambodia' },
            annotations: [
              { type: 'ORDER', text: { en: '10165' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'KH' } },
              { type: 'M49Code', text: { en: '116' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X03' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).KHM', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'KWT',
            name: { en: 'Kuwait' },
            annotations: [
              { type: 'ORDER', text: { en: '10585' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'KW' } },
              { type: 'M49Code', text: { en: '414' } },
              { type: 'ILO_REGION', text: { en: 'X36' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X05' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).KWT', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'LBN',
            name: { en: 'Lebanon' },
            annotations: [
              { type: 'ORDER', text: { en: '10605' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'LB' } },
              { type: 'M49Code', text: { en: '422' } },
              { type: 'ILO_REGION', text: { en: 'X36' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X04' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).LBN', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'MNG',
            name: { en: 'Mongolia' },
            annotations: [
              { type: 'ORDER', text: { en: '10740' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'MN' } },
              { type: 'M49Code', text: { en: '496' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X03' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).MNG', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'MYS',
            name: { en: 'Malaysia' },
            annotations: [
              { type: 'ORDER', text: { en: '10660' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'MY' } },
              { type: 'M49Code', text: { en: '458' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X04' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).MYS', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'PHL',
            name: { en: 'Philippines' },
            annotations: [
              { type: 'ORDER', text: { en: '10880' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'PH' } },
              { type: 'M49Code', text: { en: '608' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X03' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).PHL', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'THA',
            name: { en: 'Thailand' },
            annotations: [
              { type: 'ORDER', text: { en: '11105' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'TH' } },
              { type: 'M49Code', text: { en: '764' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X04' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).THA', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'TUR',
            name: { en: 'Turkey' },
            annotations: [
              { type: 'ORDER', text: { en: '11140' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'TR' } },
              { type: 'M49Code', text: { en: '792' } },
              { type: 'ILO_REGION', text: { en: 'X60' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X04' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).TUR', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'VNM',
            name: { en: 'Viet Nam' },
            annotations: [
              { type: 'ORDER', text: { en: '11215' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'VN' } },
              { type: 'M49Code', text: { en: '704' } },
              { type: 'ILO_REGION', text: { en: 'X40' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X03' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).VNM', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'YEM',
            name: { en: 'Yemen' },
            annotations: [
              { type: 'ORDER', text: { en: '11230' } },
              { type: 'Group', text: { en: 'N' } },
              { type: 'ISO2Code', text: { en: 'YE' } },
              { type: 'M49Code', text: { en: '887' } },
              { type: 'ILO_REGION', text: { en: 'X36' } },
              { type: 'WB_INCOME_GROUP', text: { en: 'X02' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).YEM', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'X06',
            name: { en: 'Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1000' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '002' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X06', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X10',
            name: { en: 'Northern Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1100' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '015' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X10', type: 'code' },
            ],
            parent: 'X06',
          },
          {
            id: 'X13',
            name: { en: 'Sub-Saharan Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1500' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '202' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X13', type: 'code' },
            ],
            parent: 'X06',
          },
          {
            id: 'X18',
            name: { en: 'Eastern Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1520' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '014' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X18', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X19',
            name: { en: 'Southern Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1530' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '018' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X19', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X21',
            name: { en: 'Americas' },
            annotations: [
              { type: 'ORDER', text: { en: '2000' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '019' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X21', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X26',
            name: { en: 'Latin America and the Caribbean' },
            annotations: [
              { type: 'ORDER', text: { en: '2500' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '419' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X26', type: 'code' },
            ],
            parent: 'X21',
          },
          {
            id: 'X31',
            name: { en: 'Caribbean' },
            annotations: [
              { type: 'ORDER', text: { en: '2510' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '029' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X31', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X32',
            name: { en: 'Central America' },
            annotations: [
              { type: 'ORDER', text: { en: '2520' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '013' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X32', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X33',
            name: { en: 'South America' },
            annotations: [
              { type: 'ORDER', text: { en: '2530' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '005' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X33', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X34',
            name: { en: 'Northern America' },
            annotations: [
              { type: 'ORDER', text: { en: '2700' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '021' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X34', type: 'code' },
            ],
            parent: 'X21',
          },
          {
            id: 'X69',
            name: { en: 'Western Europe' },
            annotations: [
              { type: 'ORDER', text: { en: '5130' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '155' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X69', type: 'code' },
            ],
            parent: 'X64',
          },
          {
            id: 'X36',
            name: { en: 'Arab States' },
            annotations: [
              { type: 'ORDER', text: { en: '3000' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X36', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X40',
            name: { en: 'Asia and the Pacific' },
            annotations: [
              { type: 'ORDER', text: { en: '4000' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X40', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X45',
            name: { en: 'Eastern Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '4100' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '030' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X45', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X20',
            name: { en: 'Western Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1540' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '011' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X20', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X49',
            name: { en: 'South-Eastern Asia and the Pacific' },
            annotations: [
              { type: 'ORDER', text: { en: '4300' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X49', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X54',
            name: { en: 'South-Eastern Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '4310' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '034' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X54', type: 'code' },
            ],
            parent: 'X49',
          },
          {
            id: 'X55',
            name: { en: 'Pacific Islands' },
            annotations: [
              { type: 'ORDER', text: { en: '4320' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X55', type: 'code' },
            ],
            parent: 'X49',
          },
          {
            id: 'X56',
            name: { en: 'Southern Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '4400' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '034' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X56', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X17',
            name: { en: 'Central Africa' },
            annotations: [
              { type: 'ORDER', text: { en: '1510' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '017' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X17', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X60',
            name: { en: 'Europe and Central Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '5000' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X60', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X64',
            name: { en: 'Northern, Southern and Western Europe' },
            annotations: [
              { type: 'ORDER', text: { en: '5100' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X64', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'X67',
            name: { en: 'Northern Europe' },
            annotations: [
              { type: 'ORDER', text: { en: '5110' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '154' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X67', type: 'code' },
            ],
            parent: 'X64',
          },
          {
            id: 'X68',
            name: { en: 'Southern Europe' },
            annotations: [
              { type: 'ORDER', text: { en: '5120' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '039' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X68', type: 'code' },
            ],
            parent: 'X64',
          },
          {
            id: 'X70',
            name: { en: 'Eastern Europe' },
            annotations: [
              { type: 'ORDER', text: { en: '5300' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '151' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X70', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'X74',
            name: { en: 'Central Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '5610' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '143' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X74', type: 'code' },
            ],
            parent: 'X78',
          },
          {
            id: 'X78',
            name: { en: 'Central and Western Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '5600' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X78', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'X79',
            name: { en: 'Western Asia' },
            annotations: [
              { type: 'ORDER', text: { en: '5620' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '145' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X79', type: 'code' },
            ],
            parent: 'X78',
          },
          {
            id: 'X04',
            name: { en: 'World: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '903' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X04', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X02',
            name: { en: 'World: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '901' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X02', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X03',
            name: { en: 'World: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '902' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X03', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X05',
            name: { en: 'World: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '904' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X05', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X01',
            name: { en: 'World' },
            annotations: [
              { type: 'ORDER', text: { en: '900' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code', text: { en: '001' } },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X01', type: 'code' },
            ],
          },
          {
            id: 'X07',
            name: { en: 'Africa: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '1001' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X07', type: 'code' },
            ],
            parent: 'X06',
          },
          {
            id: 'X08',
            name: { en: 'Africa: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '1002' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X08', type: 'code' },
            ],
            parent: 'X06',
          },
          {
            id: 'X09',
            name: { en: 'Africa: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '1003' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X09', type: 'code' },
            ],
            parent: 'X06',
          },
          {
            id: 'X11',
            name: { en: 'Northern Africa: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '1102' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X11', type: 'code' },
            ],
            parent: 'X10',
          },
          {
            id: 'X12',
            name: { en: 'Northern Africa: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '1103' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X12', type: 'code' },
            ],
            parent: 'X10',
          },
          {
            id: 'X14',
            name: { en: 'Sub-Saharan Africa: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '1501' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X14', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X15',
            name: { en: 'Sub-Saharan Africa: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '1502' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X15', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X16',
            name: { en: 'Sub-Saharan Africa: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '1503' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X16', type: 'code' },
            ],
            parent: 'X13',
          },
          {
            id: 'X22',
            name: { en: 'Americas: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '2001' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X22', type: 'code' },
            ],
            parent: 'X21',
          },
          {
            id: 'X23',
            name: { en: 'Americas: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '2002' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X23', type: 'code' },
            ],
            parent: 'X21',
          },
          {
            id: 'X24',
            name: { en: 'Americas: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '2003' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X24', type: 'code' },
            ],
            parent: 'X21',
          },
          {
            id: 'X25',
            name: { en: 'Americas: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '2004' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X25', type: 'code' },
            ],
            parent: 'X21',
          },
          {
            id: 'X27',
            name: { en: 'Latin America and the Caribbean: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '2501' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X27', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X28',
            name: { en: 'Latin America and the Caribbean: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '2502' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X28', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X29',
            name: { en: 'Latin America and the Caribbean: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '2503' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X29', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X30',
            name: { en: 'Latin America and the Caribbean: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '2504' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X30', type: 'code' },
            ],
            parent: 'X26',
          },
          {
            id: 'X35',
            name: { en: 'Northern America: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '2704' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X35', type: 'code' },
            ],
            parent: 'X34',
          },
          {
            id: 'X37',
            name: { en: 'Arab States: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '3002' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X37', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'X38',
            name: { en: 'Arab States: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '3003' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X38', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'X39',
            name: { en: 'Arab States: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '3004' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X39', type: 'code' },
            ],
            parent: 'X36',
          },
          {
            id: 'X41',
            name: { en: 'Asia and the Pacific: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '4001' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X41', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X42',
            name: { en: 'Asia and the Pacific: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4002' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X42', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X43',
            name: { en: 'Asia and the Pacific: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4003' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X43', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X44',
            name: { en: 'Asia and the Pacific: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '4004' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X44', type: 'code' },
            ],
            parent: 'X40',
          },
          {
            id: 'X46',
            name: { en: 'Eastern Asia: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '4101' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X46', type: 'code' },
            ],
            parent: 'X45',
          },
          {
            id: 'X47',
            name: { en: 'Eastern Asia: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4103' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X47', type: 'code' },
            ],
            parent: 'X45',
          },
          {
            id: 'X48',
            name: { en: 'Eastern Asia: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '4104' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X48', type: 'code' },
            ],
            parent: 'X45',
          },
          {
            id: 'X50',
            name: { en: 'South-Eastern Asia and the Pacific: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '4301' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X50', type: 'code' },
            ],
            parent: 'X49',
          },
          {
            id: 'X51',
            name: { en: 'South-Eastern Asia and the Pacific: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4302' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X51', type: 'code' },
            ],
            parent: 'X49',
          },
          {
            id: 'X52',
            name: { en: 'South-Eastern Asia and the Pacific: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4303' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X52', type: 'code' },
            ],
            parent: 'X49',
          },
          {
            id: 'X53',
            name: { en: 'South-Eastern Asia and the Pacific: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '4304' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X53', type: 'code' },
            ],
            parent: 'X49',
          },
          {
            id: 'X57',
            name: { en: 'Southern Asia: Low income' },
            annotations: [
              { type: 'ORDER', text: { en: '4401' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X57', type: 'code' },
            ],
            parent: 'X56',
          },
          {
            id: 'X58',
            name: { en: 'Southern Asia: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4402' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X58', type: 'code' },
            ],
            parent: 'X56',
          },
          {
            id: 'X59',
            name: { en: 'Southern Asia: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4403' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X59', type: 'code' },
            ],
            parent: 'X56',
          },
          {
            id: 'X61',
            name: { en: 'Europe and Central Asia: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5002' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X61', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'X62',
            name: { en: 'Europe and Central Asia: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5003' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X62', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'X63',
            name: { en: 'Europe and Central Asia: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '5004' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X63', type: 'code' },
            ],
            parent: 'X60',
          },
          {
            id: 'X65',
            name: { en: 'Northern, Southern and Western Europe: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5103' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X65', type: 'code' },
            ],
            parent: 'X64',
          },
          {
            id: 'X66',
            name: { en: 'Northern, Southern and Western Europe: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '5104' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X66', type: 'code' },
            ],
            parent: 'X64',
          },
          {
            id: 'X71',
            name: { en: 'Eastern Europe: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5302' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X71', type: 'code' },
            ],
            parent: 'X70',
          },
          {
            id: 'X72',
            name: { en: 'Eastern Europe: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5303' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X72', type: 'code' },
            ],
            parent: 'X70',
          },
          {
            id: 'X73',
            name: { en: 'Eastern Europe: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '5304' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X73', type: 'code' },
            ],
            parent: 'X70',
          },
          {
            id: 'X75',
            name: { en: 'Central and Western Asia: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5602' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X75', type: 'code' },
            ],
            parent: 'X78',
          },
          {
            id: 'X76',
            name: { en: 'Central and Western Asia: Upper-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '5603' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X76', type: 'code' },
            ],
            parent: 'X78',
          },
          {
            id: 'X77',
            name: { en: 'Central and Western Asia: High income' },
            annotations: [
              { type: 'ORDER', text: { en: '5604' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X77', type: 'code' },
            ],
            parent: 'X78',
          },
          {
            id: 'X84',
            name: { en: 'ASEAN' },
            annotations: [
              { type: 'ORDER', text: { en: '6000' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X84', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X85',
            name: { en: 'BRICS' },
            annotations: [
              { type: 'ORDER', text: { en: '6010' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X85', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X82',
            name: { en: 'European Union 28' },
            annotations: [
              { type: 'ORDER', text: { en: '6020' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X82', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X83',
            name: { en: 'G20' },
            annotations: [
              { type: 'ORDER', text: { en: '6030' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X83', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X86',
            name: { en: 'Eastern Asia: Lower-middle income' },
            annotations: [
              { type: 'ORDER', text: { en: '4102' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X86', type: 'code' },
            ],
            parent: 'X45',
          },
          {
            id: 'X87',
            name: { en: 'World excluding BRICS' },
            annotations: [
              { type: 'ORDER', text: { en: '6040' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X87', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X88',
            name: { en: 'G7' },
            annotations: [
              { type: 'ORDER', text: { en: '6050' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X88', type: 'code' },
            ],
            parent: 'X01',
          },
          {
            id: 'X89',
            name: { en: 'MENA' },
            annotations: [
              { type: 'ORDER', text: { en: '6060' } },
              { type: 'Group', text: { en: 'Y' } },
              { type: 'ISO2Code' },
              { type: 'M49Code' },
              { type: 'ILO_REGION' },
              { type: 'WB_INCOME_GROUP' },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_AREA(1.0).X89', type: 'code' },
            ],
            parent: 'X01',
          },
        ],
      },
      {
        id: 'CL_COLLECTION',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_COLLECTION(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Collection' },
        isPartial: true,
        codes: [
          {
            id: 'ILMS',
            name: { en: 'Labour Migration Statistics' },
            annotations: [{ type: 'ORDER', text: { en: '140' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_COLLECTION(1.0).ILMS',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_FREQ',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_FREQ(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'FREQ' },
        isPartial: true,
        codes: [
          {
            id: 'A',
            name: { en: 'Annual' },
            annotations: [{ type: 'ORDER', text: { en: '20' } }],
            links: [{ rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_FREQ(1.0).A', type: 'code' }],
          },
        ],
      },
      {
        id: 'CL_INDICATOR',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_INDICATOR(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Indicator' },
        isPartial: true,
        codes: [
          {
            id: 'MFL_TEMP_OCU_NB',
            name: { en: 'Inflow of employed migrants by occupation' },
            description: {
              en:
                'Inflow of employed migrants refer to the number of persons who changed their country of usual residence and were also employed during a specified brief period. Data are disaggregated by occupation according to the latest version of the International Standard Classification of Occupations (ISCO-08). Information on occupation provides a description of the set of tasks and duties which are carried out by, or can be assigned to, one person.',
            },
            annotations: [{ type: 'ORDER', text: { en: '1860' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_INDICATOR(1.0).MFL_TEMP_OCU_NB',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_NOTE_C6',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_C6(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Nonstandard age group' },
        annotations: [
          { type: 'DISPLAY_MODE', text: { en: 'FOOT' } },
          { type: 'Note_Group', text: { en: 'CLA_NOTE' } },
        ],
        isPartial: true,
        codes: [
          {
            id: '2351',
            name: { en: 'Excluding ages 65+' },
            annotations: [
              { type: 'ORDER', text: { en: '96599' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_C6(1.0).2351', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_NOTE_I11',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_I11(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Break in series' },
        annotations: [
          { type: 'DISPLAY_MODE', text: { en: 'ALL' } },
          { type: 'Note_Group', text: { en: 'IND_NOTE' } },
        ],
        isPartial: true,
        codes: [
          {
            id: '266',
            name: { en: 'Other or unspecified type of break' },
            annotations: [
              { type: 'ORDER', text: { en: '99' } },
              { type: 'Is_Default', text: { en: 'N' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_I11(1.0).266', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_NOTE_S3',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_S3(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Data reference period' },
        annotations: [
          { type: 'DISPLAY_MODE', text: { en: 'META' } },
          { type: 'Note_Group', text: { en: 'SRC_NOTE' } },
        ],
        isPartial: true,
        codes: [
          {
            id: '17',
            name: { en: 'June' },
            annotations: [
              { type: 'ORDER', text: { en: '170' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_S3(1.0).17', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_NOTE_S9',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_S9(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Reference group coverage' },
        annotations: [
          { type: 'DISPLAY_MODE', text: { en: 'ALL' } },
          { type: 'Note_Group', text: { en: 'SRC_NOTE' } },
        ],
        isPartial: true,
        codes: [
          {
            id: '66',
            name: { en: 'Employees' },
            annotations: [
              { type: 'ORDER', text: { en: '20' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_S9(1.0).66', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_NOTE_T2',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_T2(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Age coverage - minimum age' },
        annotations: [
          { type: 'DISPLAY_MODE', text: { en: 'META' } },
          { type: 'Note_Group', text: { en: 'SRC_NOTE' } },
        ],
        isPartial: true,
        codes: [
          {
            id: '85',
            name: { en: '16 years old' },
            annotations: [
              { type: 'ORDER', text: { en: '70' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_T2(1.0).85', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_NOTE_T36',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_T36(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Definition of migrants' },
        annotations: [
          { type: 'DISPLAY_MODE', text: { en: 'META' } },
          { type: 'Note_Group', text: { en: 'IND_NOTE' } },
        ],
        isPartial: true,
        codes: [
          {
            id: '3214',
            name: { en: 'Persons born outside of the country' },
            annotations: [
              { type: 'ORDER', text: { en: '10' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_T36(1.0).3214', type: 'code' },
            ],
          },
          {
            id: '3223',
            name: { en: 'Persons residing abroad one year ago' },
            annotations: [
              { type: 'ORDER', text: { en: '20' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_T36(1.0).3223', type: 'code' },
            ],
          },
          {
            id: '3218',
            name: { en: 'Registered migrant workers' },
            annotations: [
              { type: 'ORDER', text: { en: '40' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_T36(1.0).3218', type: 'code' },
            ],
          },
          {
            id: '3215',
            name: { en: 'Non-citizens' },
            annotations: [
              { type: 'ORDER', text: { en: '80' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_T36(1.0).3215', type: 'code' },
            ],
          },
          {
            id: '4111',
            name: { en: 'Foreigners holding a 3 months or more residence/work permit valid' },
            annotations: [
              { type: 'ORDER', text: { en: '90' } },
              { type: 'Is_Default', text: { en: 'Y' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_NOTE_T36(1.0).4111', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_OBS_STATUS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_OBS_STATUS(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'OBS_STATUS' },
        isPartial: false,
        codes: [
          {
            id: 'B',
            name: { en: 'Break in series' },
            annotations: [{ type: 'ORDER', text: { en: '1' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).B', type: 'code' },
            ],
          },
          {
            id: 'L',
            name: { en: 'Computed value' },
            annotations: [{ type: 'ORDER', text: { en: '10' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).L', type: 'code' },
            ],
          },
          {
            id: 'C',
            name: { en: 'Confidential' },
            annotations: [{ type: 'ORDER', text: { en: '20' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).C', type: 'code' },
            ],
          },
          {
            id: 'E',
            name: { en: 'Estimate' },
            annotations: [{ type: 'ORDER', text: { en: '30' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).E', type: 'code' },
            ],
          },
          {
            id: 'F',
            name: { en: 'Forecast' },
            annotations: [{ type: 'ORDER', text: { en: '40' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).F', type: 'code' },
            ],
          },
          {
            id: 'X',
            name: { en: 'Not applicable' },
            annotations: [{ type: 'ORDER', text: { en: '80' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).X', type: 'code' },
            ],
          },
          {
            id: 'N',
            name: { en: 'Not available' },
            annotations: [{ type: 'ORDER', text: { en: '90' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).N', type: 'code' },
            ],
          },
          {
            id: 'P',
            name: { en: 'Provisional' },
            annotations: [{ type: 'ORDER', text: { en: '100' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).P', type: 'code' },
            ],
          },
          {
            id: 'S',
            name: { en: 'Not significant' },
            annotations: [{ type: 'ORDER', text: { en: '110' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).S', type: 'code' },
            ],
          },
          {
            id: 'U',
            name: { en: 'Unreliable' },
            annotations: [{ type: 'ORDER', text: { en: '120' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).U', type: 'code' },
            ],
          },
          {
            id: 'R',
            name: { en: 'Real value' },
            annotations: [{ type: 'ORDER', text: { en: '130' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).R', type: 'code' },
            ],
          },
          {
            id: 'Z',
            name: { en: 'Non-official estimated value' },
            annotations: [{ type: 'ORDER', text: { en: '150' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).Z', type: 'code' },
            ],
          },
          {
            id: 'I',
            name: { en: 'Imputation' },
            annotations: [{ type: 'ORDER', text: { en: '160' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OBS_STATUS(1.0).I', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_OCU',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_OCU(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Classification: OCU' },
        isPartial: true,
        codes: [
          {
            id: 'OCU_SKILL_TOTAL',
            name: { en: 'Total' },
            annotations: [
              { type: 'ORDER', text: { en: '1' } },
              { type: 'Is_Total', text: { en: 'Y' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_SKILL_TOTAL',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_SKILL_L3-4',
            name: { en: 'Skill levels 3 and 4 (high)' },
            annotations: [
              { type: 'ORDER', text: { en: '2' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_SKILL_L3-4',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_SKILL_L2',
            name: { en: 'Skill level 2 (medium)' },
            annotations: [
              { type: 'ORDER', text: { en: '3' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_SKILL_L2',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_SKILL_L1',
            name: { en: 'Skill level 1 (low)' },
            annotations: [
              { type: 'ORDER', text: { en: '4' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_SKILL_L1',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_SKILL_X',
            name: { en: 'Not elsewhere classified' },
            annotations: [
              { type: 'ORDER', text: { en: '6' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_SKILL_X',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_TOTAL',
            name: { en: 'Total' },
            annotations: [
              { type: 'ORDER', text: { en: '14020' } },
              { type: 'Is_Total', text: { en: 'Y' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_TOTAL',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_1',
            name: { en: 'Managers' },
            annotations: [
              { type: 'ORDER', text: { en: '14030' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_1',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_1_11',
            name: { en: 'Chief executives, senior officials and legislators' },
            annotations: [
              { type: 'ORDER', text: { en: '14040' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_1_11',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_1_12',
            name: { en: 'Administrative and commercial managers' },
            annotations: [
              { type: 'ORDER', text: { en: '14050' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_1_12',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_1_14',
            name: { en: 'Hospitality, retail and other services managers' },
            annotations: [
              { type: 'ORDER', text: { en: '14060' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_1_14',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_2',
            name: { en: 'Professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14070' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_2',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_2_22',
            name: { en: 'Health professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14080' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_2_22',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_2_23',
            name: { en: 'Teaching professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14090' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_2_23',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_2_25',
            name: { en: 'Information and communications technology professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14100' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_2_25',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_3',
            name: { en: 'Technicians and associate professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14110' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_3',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_3_32',
            name: { en: 'Health associate professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14120' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_3_32',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_3_33',
            name: { en: 'Business and administration associate professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14130' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_3_33',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_4',
            name: { en: 'Clerical support workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14140' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_4',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_4_41',
            name: { en: 'General and keyboard clerks' },
            annotations: [
              { type: 'ORDER', text: { en: '14150' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_4_41',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_4_42',
            name: { en: 'Customer services clerks' },
            annotations: [
              { type: 'ORDER', text: { en: '14160' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_4_42',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_5',
            name: { en: 'Service and sales workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14170' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_5',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_5_51',
            name: { en: 'Personal service workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14180' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_5_51',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_5_52',
            name: { en: 'Sales workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14190' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_5_52',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_6',
            name: { en: 'Skilled agricultural, forestry and fishery workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14200' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_6',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_6_61',
            name: { en: 'Market oriented skilled agricultural workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14210' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_6_61',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_6_63',
            name: { en: 'Subsistence farmers, fishers, hunters and gatherers' },
            annotations: [
              { type: 'ORDER', text: { en: '14220' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_6_63',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_7',
            name: { en: 'Craft and related trades workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14230' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_7',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_7_71',
            name: { en: 'Building and related trades workers, excluding electricians' },
            annotations: [
              { type: 'ORDER', text: { en: '14240' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_7_71',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_7_74',
            name: { en: 'Electrical and electronic trades workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14250' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_7_74',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_8',
            name: { en: 'Plant and machine operators, and assemblers' },
            annotations: [
              { type: 'ORDER', text: { en: '14260' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_8',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_8_81',
            name: { en: 'Stationary plant and machine operators' },
            annotations: [
              { type: 'ORDER', text: { en: '14270' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_8_81',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_8_83',
            name: { en: 'Drivers and mobile plant operators' },
            annotations: [
              { type: 'ORDER', text: { en: '14280' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_8_83',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_9',
            name: { en: 'Elementary occupations' },
            annotations: [
              { type: 'ORDER', text: { en: '14290' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_9',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_9_91',
            name: { en: 'Cleaners and helpers' },
            annotations: [
              { type: 'ORDER', text: { en: '14300' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_9_91',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_9_92',
            name: { en: 'Agricultural, forestry and fishery labourers' },
            annotations: [
              { type: 'ORDER', text: { en: '14310' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_9_92',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_9_93',
            name: { en: 'Labourers in mining, construction, manufacturing and transport' },
            annotations: [
              { type: 'ORDER', text: { en: '14320' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_9_93',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_0',
            name: { en: 'Armed forces occupations' },
            annotations: [
              { type: 'ORDER', text: { en: '14330' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_0',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO08_X',
            name: { en: 'Not elsewhere classified' },
            annotations: [
              { type: 'ORDER', text: { en: '14340' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO08_X',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_TOTAL',
            name: { en: 'Total' },
            annotations: [
              { type: 'ORDER', text: { en: '14350' } },
              { type: 'Is_Total', text: { en: 'Y' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_TOTAL',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_1',
            name: { en: 'Legislators, senior officials and managers' },
            annotations: [
              { type: 'ORDER', text: { en: '14360' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_1',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_1_11',
            name: { en: 'Legislators and senior officials' },
            annotations: [
              { type: 'ORDER', text: { en: '14370' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_1_11',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_1_13',
            name: { en: 'General managers' },
            annotations: [
              { type: 'ORDER', text: { en: '14380' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_1_13',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_2',
            name: { en: 'Professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14390' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_2',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_2_23',
            name: { en: 'Teaching professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14400' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_2_23',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_3',
            name: { en: 'Technicians and associate professionals' },
            annotations: [
              { type: 'ORDER', text: { en: '14410' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_3',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_4',
            name: { en: 'Clerks' },
            annotations: [
              { type: 'ORDER', text: { en: '14430' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_4',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_5',
            name: { en: 'Service workers and shop and market sales workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14460' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_5',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_6',
            name: { en: 'Skilled agricultural and fishery workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14490' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_6',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_7',
            name: { en: 'Craft and related trades workers' },
            annotations: [
              { type: 'ORDER', text: { en: '14510' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_7',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_8',
            name: { en: 'Plant and machine operators and assemblers' },
            annotations: [
              { type: 'ORDER', text: { en: '14520' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_8',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_9',
            name: { en: 'Elementary occupations' },
            annotations: [
              { type: 'ORDER', text: { en: '14550' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_9',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_0',
            name: { en: 'Armed forces' },
            annotations: [
              { type: 'ORDER', text: { en: '14590' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_0',
                type: 'code',
              },
            ],
          },
          {
            id: 'OCU_ISCO88_X',
            name: { en: 'Not elsewhere classified' },
            annotations: [
              { type: 'ORDER', text: { en: '14600' } },
              { type: 'Is_Total', text: { en: 'N' } },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_OCU(1.0).OCU_ISCO88_X',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_REPRESENTED_VARIABLE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_REPRESENTED_VARIABLE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Represented Variable' },
        isPartial: true,
        codes: [
          {
            id: 'MFL_TEMP_NB',
            name: { en: 'Inflows of employed migrants' },
            annotations: [{ type: 'ORDER', text: { en: '1260' } }, { type: 'Complement' }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_REPRESENTED_VARIABLE(1.0).MFL_TEMP_NB',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_SOURCE',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_SOURCE(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Source' },
        isPartial: false,
        codes: [
          {
            id: 'BE',
            name: { en: 'Labour force survey - adjusted' },
            annotations: [{ type: 'ORDER', text: { en: '19' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).BE', type: 'code' },
            ],
          },
          {
            id: 'BA',
            name: { en: 'Labour force survey' },
            annotations: [{ type: 'ORDER', text: { en: '20' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).BA', type: 'code' },
            ],
          },
          {
            id: 'BB',
            name: { en: 'Household income/expenditure survey' },
            annotations: [{ type: 'ORDER', text: { en: '30' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).BB', type: 'code' },
            ],
          },
          {
            id: 'BC',
            name: { en: 'Child labour survey' },
            annotations: [{ type: 'ORDER', text: { en: '32' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).BC', type: 'code' },
            ],
          },
          {
            id: 'BX',
            name: { en: 'Other household survey' },
            annotations: [{ type: 'ORDER', text: { en: '40' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).BX', type: 'code' },
            ],
          },
          {
            id: 'AA',
            name: { en: 'Population census' },
            annotations: [{ type: 'ORDER', text: { en: '41' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).AA', type: 'code' },
            ],
          },
          {
            id: 'CA',
            name: { en: 'Economic or establishment census' },
            annotations: [{ type: 'ORDER', text: { en: '50' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).CA', type: 'code' },
            ],
          },
          {
            id: 'DA',
            name: { en: 'Establishment survey' },
            annotations: [{ type: 'ORDER', text: { en: '60' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).DA', type: 'code' },
            ],
          },
          {
            id: 'EA',
            name: { en: 'Official estimate' },
            annotations: [{ type: 'ORDER', text: { en: '80' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).EA', type: 'code' },
            ],
          },
          {
            id: 'EB',
            name: { en: 'Other official source' },
            annotations: [{ type: 'ORDER', text: { en: '85' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).EB', type: 'code' },
            ],
          },
          {
            id: 'FA',
            name: { en: 'Insurance records' },
            annotations: [{ type: 'ORDER', text: { en: '90' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FA', type: 'code' },
            ],
          },
          {
            id: 'FB',
            name: { en: 'Employment office records' },
            annotations: [{ type: 'ORDER', text: { en: '100' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FB', type: 'code' },
            ],
          },
          {
            id: 'FE',
            name: { en: 'Collective agreements' },
            annotations: [{ type: 'ORDER', text: { en: '110' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FE', type: 'code' },
            ],
          },
          {
            id: 'FF',
            name: { en: 'Labour inspectorate records' },
            annotations: [{ type: 'ORDER', text: { en: '120' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FF', type: 'code' },
            ],
          },
          {
            id: 'FH',
            name: { en: "Records of employers' organizations" },
            annotations: [{ type: 'ORDER', text: { en: '130' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FH', type: 'code' },
            ],
          },
          {
            id: 'FI',
            name: { en: "Records of workers' organizations" },
            annotations: [{ type: 'ORDER', text: { en: '140' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FI', type: 'code' },
            ],
          },
          {
            id: 'FJ',
            name: { en: 'Population register' },
            annotations: [{ type: 'ORDER', text: { en: '150' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FJ', type: 'code' },
            ],
          },
          {
            id: 'FK',
            name: { en: 'Establishment or business register' },
            annotations: [{ type: 'ORDER', text: { en: '160' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FK', type: 'code' },
            ],
          },
          {
            id: 'FX',
            name: { en: 'Other administrative records and related sources' },
            annotations: [{ type: 'ORDER', text: { en: '170' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).FX', type: 'code' },
            ],
          },
          {
            id: 'GA',
            name: { en: 'Consumer price survey' },
            annotations: [{ type: 'ORDER', text: { en: '180' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).GA', type: 'code' },
            ],
          },
          {
            id: 'JA',
            name: { en: 'National Accounts' },
            annotations: [{ type: 'ORDER', text: { en: '190' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).JA', type: 'code' },
            ],
          },
          {
            id: 'XA',
            name: { en: 'ILO estimate' },
            annotations: [{ type: 'ORDER', text: { en: '200' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).XA', type: 'code' },
            ],
          },
          {
            id: 'XX',
            name: { en: 'Other source' },
            annotations: [{ type: 'ORDER', text: { en: '210' } }, { type: 'ACRONYM' }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SOURCE(1.0).XX', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_SURVEY',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_SURVEY(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Survey' },
        isPartial: true,
        codes: [
          {
            id: '544',
            name: { en: 'Labour Force Survey' },
            annotations: [
              { type: 'ORDER', text: { en: '5' } },
              { type: 'CountryCode', text: { en: 'MYS' } },
              { type: 'SourceCode', text: { en: 'BA' } },
              { type: 'SourceType', text: { en: 'LFS' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).544', type: 'code' },
            ],
          },
          {
            id: '6649',
            name: { en: 'Labour Force Survey and Migration Survey' },
            annotations: [
              { type: 'ORDER', text: { en: '10' } },
              { type: 'CountryCode', text: { en: 'THA' } },
              { type: 'SourceCode', text: { en: 'BA' } },
              { type: 'SourceType', text: { en: 'LFS' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).6649', type: 'code' },
            ],
          },
          {
            id: '858',
            name: { en: 'Population census' },
            annotations: [
              { type: 'ORDER', text: { en: '10' } },
              { type: 'CountryCode', text: { en: 'ARE' } },
              { type: 'SourceCode', text: { en: 'AA' } },
              { type: 'SourceType', text: { en: 'PC' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).858', type: 'code' },
            ],
          },
          {
            id: '3080',
            name: { en: 'Living Conditions Survey' },
            annotations: [
              { type: 'ORDER', text: { en: '15' } },
              { type: 'CountryCode', text: { en: 'LBN' } },
              { type: 'SourceCode', text: { en: 'BB' } },
              { type: 'SourceType', text: { en: 'HIES' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).3080', type: 'code' },
            ],
          },
          {
            id: '12920',
            name: { en: 'Ministry of Labour Data Library' },
            annotations: [
              { type: 'ORDER', text: { en: '30' } },
              { type: 'CountryCode', text: { en: 'ARE' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12920', type: 'code' },
            ],
          },
          {
            id: '1320',
            name: { en: 'Population census' },
            annotations: [
              { type: 'ORDER', text: { en: '30' } },
              { type: 'CountryCode', text: { en: 'KHM' } },
              { type: 'SourceCode', text: { en: 'AA' } },
              { type: 'SourceType', text: { en: 'PC' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).1320', type: 'code' },
            ],
          },
          {
            id: '12972',
            name: { en: 'Annual report of tourism and migration, Fiji Bureau of Statistics' },
            annotations: [
              { type: 'ORDER', text: { en: '35' } },
              { type: 'CountryCode', text: { en: 'FJI' } },
              { type: 'SourceCode', text: { en: 'EB' } },
              { type: 'SourceType', text: { en: 'Other' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12972', type: 'code' },
            ],
          },
          {
            id: '12919',
            name: { en: 'Ministry of Labour Statistics' },
            annotations: [
              { type: 'ORDER', text: { en: '40' } },
              { type: 'CountryCode', text: { en: 'LBN' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12919', type: 'code' },
            ],
          },
          {
            id: '12914',
            name: { en: 'Administrative Records from the CSB Annual Statistical Abstract' },
            annotations: [
              { type: 'ORDER', text: { en: '45' } },
              { type: 'CountryCode', text: { en: 'KWT' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12914', type: 'code' },
            ],
          },
          {
            id: '12966',
            name: {
              en:
                'Statistical coupon for migration on paper, drawn up in the registration and removal of citizens of the Kyrgyz Republic to the register at the place of residence.',
            },
            annotations: [
              { type: 'ORDER', text: { en: '45' } },
              { type: 'CountryCode', text: { en: 'KGZ' } },
              { type: 'SourceCode', text: { en: 'EA' } },
              { type: 'SourceType', text: { en: 'OE' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12966', type: 'code' },
            ],
          },
          {
            id: '12913',
            name: { en: 'Administrative Records from the CSO Statistical Yearbook' },
            annotations: [
              { type: 'ORDER', text: { en: '55' } },
              { type: 'CountryCode', text: { en: 'YEM' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12913', type: 'code' },
            ],
          },
          {
            id: '6663',
            name: {
              en: 'Administrative records of the Bureau of Local Employment, Department of Labour and Employment',
            },
            annotations: [
              { type: 'ORDER', text: { en: '55' } },
              { type: 'CountryCode', text: { en: 'PHL' } },
              { type: 'SourceCode', text: { en: 'FB' } },
              { type: 'SourceType', text: { en: 'ADM-EOR' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).6663', type: 'code' },
            ],
          },
          {
            id: '6664',
            name: { en: 'Administrative records of the Department of Employment, Ministry of Labour' },
            annotations: [
              { type: 'ORDER', text: { en: '55' } },
              { type: 'CountryCode', text: { en: 'THA' } },
              { type: 'SourceCode', text: { en: 'FB' } },
              { type: 'SourceType', text: { en: 'ADM-EOR' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).6664', type: 'code' },
            ],
          },
          {
            id: '13332',
            name: { en: 'Administrative records' },
            annotations: [
              { type: 'ORDER', text: { en: '60' } },
              { type: 'CountryCode', text: { en: 'MNG' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).13332', type: 'code' },
            ],
          },
          {
            id: '12959',
            name: { en: 'Administrative records of the Bureau of Employment' },
            annotations: [
              { type: 'ORDER', text: { en: '65' } },
              { type: 'CountryCode', text: { en: 'VNM' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).12959', type: 'code' },
            ],
          },
          {
            id: '507',
            name: { en: 'Records from the Ministry of Labour and Social Security' },
            annotations: [
              { type: 'ORDER', text: { en: '80' } },
              { type: 'CountryCode', text: { en: 'TUR' } },
              { type: 'SourceCode', text: { en: 'FX' } },
              { type: 'SourceType', text: { en: 'ADM' } },
            ],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_SURVEY(1.0).507', type: 'code' },
            ],
          },
        ],
      },
      {
        id: 'CL_UNIT_MEASURE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MEASURE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'UNIT_MEASURE' },
        isPartial: false,
        codes: [
          {
            id: 'PS',
            name: { en: 'Persons' },
            annotations: [{ type: 'ORDER', text: { en: '20' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).PS',
                type: 'code',
              },
            ],
          },
          {
            id: 'JS',
            name: { en: 'Jobs' },
            annotations: [{ type: 'ORDER', text: { en: '30' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).JS',
                type: 'code',
              },
            ],
          },
          {
            id: 'MN',
            name: { en: 'Minutes' },
            annotations: [{ type: 'ORDER', text: { en: '35' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).MN',
                type: 'code',
              },
            ],
          },
          {
            id: 'HR',
            name: { en: 'Hours' },
            annotations: [{ type: 'ORDER', text: { en: '40' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).HR',
                type: 'code',
              },
            ],
          },
          {
            id: 'DS',
            name: { en: 'Days' },
            annotations: [{ type: 'ORDER', text: { en: '50' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).DS',
                type: 'code',
              },
            ],
          },
          {
            id: 'WK',
            name: { en: 'Weeks' },
            annotations: [{ type: 'ORDER', text: { en: '60' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).WK',
                type: 'code',
              },
            ],
          },
          {
            id: 'MH',
            name: { en: 'Months' },
            annotations: [{ type: 'ORDER', text: { en: '70' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).MH',
                type: 'code',
              },
            ],
          },
          {
            id: 'YR',
            name: { en: 'Years' },
            annotations: [{ type: 'ORDER', text: { en: '80' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).YR',
                type: 'code',
              },
            ],
          },
          {
            id: 'CS',
            name: { en: 'Cases' },
            annotations: [{ type: 'ORDER', text: { en: '85' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).CS',
                type: 'code',
              },
            ],
          },
          {
            id: 'CR',
            name: { en: 'Currency' },
            annotations: [{ type: 'ORDER', text: { en: '89' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).CR',
                type: 'code',
              },
            ],
          },
          {
            id: 'LC',
            name: { en: 'Local currency' },
            annotations: [{ type: 'ORDER', text: { en: '90' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).LC',
                type: 'code',
              },
            ],
          },
          {
            id: 'PP',
            name: { en: 'US Dollar converted using PPPs' },
            annotations: [{ type: 'ORDER', text: { en: '100' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).PP',
                type: 'code',
              },
            ],
          },
          {
            id: 'HH',
            name: { en: 'Households' },
            annotations: [{ type: 'ORDER', text: { en: '110' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).HH',
                type: 'code',
              },
            ],
          },
          {
            id: 'XT',
            name: { en: 'Per 1000 workers' },
            annotations: [{ type: 'ORDER', text: { en: '130' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).XT',
                type: 'code',
              },
            ],
          },
          {
            id: 'WP',
            name: { en: 'Workplaces' },
            annotations: [{ type: 'ORDER', text: { en: '180' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).WP',
                type: 'code',
              },
            ],
          },
          {
            id: 'PT',
            name: { en: 'Percentage' },
            annotations: [{ type: 'ORDER', text: { en: '190' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).PT',
                type: 'code',
              },
            ],
          },
          {
            id: 'RT',
            name: { en: 'Rate' },
            annotations: [{ type: 'ORDER', text: { en: '200' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).RT',
                type: 'code',
              },
            ],
          },
          {
            id: 'IN',
            name: { en: 'Base year = 100' },
            annotations: [{ type: 'ORDER', text: { en: '210' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).IN',
                type: 'code',
              },
            ],
          },
          {
            id: 'US',
            name: { en: 'US Dollar' },
            annotations: [{ type: 'ORDER', text: { en: '220' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE(1.0).US',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_UNIT_MEASURE_TYPE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MEASURE_TYPE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'UNIT_MEASURE_TYPE' },
        isPartial: false,
        codes: [
          {
            id: 'NB',
            name: { en: 'Number' },
            annotations: [{ type: 'ORDER', text: { en: '20' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE_TYPE(1.0).NB',
                type: 'code',
              },
            ],
          },
          {
            id: 'RT',
            name: { en: 'Rate' },
            annotations: [{ type: 'ORDER', text: { en: '30' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE_TYPE(1.0).RT',
                type: 'code',
              },
            ],
          },
          {
            id: 'IN',
            name: { en: 'Index' },
            annotations: [{ type: 'ORDER', text: { en: '50' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE_TYPE(1.0).IN',
                type: 'code',
              },
            ],
          },
          {
            id: 'DT',
            name: { en: 'Distribution' },
            annotations: [{ type: 'ORDER', text: { en: '80' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE_TYPE(1.0).DT',
                type: 'code',
              },
            ],
          },
          {
            id: 'GR',
            name: { en: 'Annual growth rate' },
            annotations: [{ type: 'ORDER', text: { en: '90' } }],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MEASURE_TYPE(1.0).GR',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_UNIT_MULT',
        links: [
          { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MULT(1.0)', type: 'codelist' },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'UNIT_MULT' },
        isPartial: false,
        codes: [
          {
            id: '0',
            name: { en: 'Units' },
            annotations: [{ type: 'ORDER', text: { en: '10' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MULT(1.0).0', type: 'code' },
            ],
          },
          {
            id: '3',
            name: { en: 'Thousands' },
            annotations: [{ type: 'ORDER', text: { en: '40' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MULT(1.0).3', type: 'code' },
            ],
          },
          {
            id: '6',
            name: { en: 'Millions' },
            annotations: [{ type: 'ORDER', text: { en: '70' } }],
            links: [
              { rel: 'self', urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ILO:CL_UNIT_MULT(1.0).6', type: 'code' },
            ],
          },
        ],
      },
    ],
    dataStructures: [
      {
        id: 'ILMS_ALL_MFL_TEMP_OCU_NB',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
            type: 'datastructure',
          },
        ],
        version: '1.0',
        agencyID: 'ILO',
        isExternalReference: false,
        isFinal: true,
        name: { en: 'Inflow of employed migrants by occupation (thousands)' },
        description: {
          en:
            'Inflow of employed migrants refer to the number of persons who changed their country of usual residence and were also employed during a specified brief period. Data are disaggregated by occupation according to the latest version of the International Standard Classification of Occupations (ISCO-08). Information on occupation provides a description of the set of tasks and duties which are carried out by, or can be assigned to, one person.',
        },
        dataStructureComponents: {
          attributeList: {
            id: 'AttributeDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.AttributeDescriptor=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).AttributeDescriptor',
                type: 'attributedescriptor',
              },
            ],
            attributes: [
              {
                id: 'S3',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).S3',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).S3',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_S3(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'C6',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).C6',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).C6',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_C6(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'S9',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).S9',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).S9',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_S9(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'I11',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).I11',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).I11',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_I11(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'T2',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).T2',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).T2',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_T2(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'T36',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).T36',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_NOTE_TYPE(1.0).T36',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_NOTE_T36(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'OBS_STATUS',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).OBS_STATUS',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).OBS_STATUS',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_OBS_STATUS(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'UNIT_MEASURE_TYPE',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).UNIT_MEASURE_TYPE',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).UNIT_MEASURE_TYPE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MEASURE_TYPE(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'UNIT_MEASURE',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).UNIT_MEASURE',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).UNIT_MEASURE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MEASURE(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'UNIT_MULT',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).UNIT_MULT',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).UNIT_MULT',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_UNIT_MULT(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'FREE_TEXT_NOTE',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).FREE_TEXT_NOTE',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).FREE_TEXT_NOTE',
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'DECIMALS',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).DECIMALS',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).DECIMALS',
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'SOURCE',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).SOURCE',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).SOURCE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_SOURCE(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
              {
                id: 'INDICATOR',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).INDICATOR',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).INDICATOR',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_INDICATOR(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: { primaryMeasure: 'OBS_VALUE' },
              },
            ],
          },
          dimensionList: {
            id: 'DimensionDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.DimensionDescriptor=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).DimensionDescriptor',
                type: 'dimensiondescriptor',
              },
            ],
            dimensions: [
              {
                id: 'COLLECTION',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).COLLECTION',
                    type: 'dimension',
                  },
                ],
                position: 0,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).COLLECTION',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_COLLECTION(1.0)',
                },
              },
              {
                id: 'REF_AREA',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).REF_AREA',
                    type: 'dimension',
                  },
                ],
                position: 1,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).REF_AREA',
                localRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_AREA(1.0)' },
              },
              {
                id: 'FREQ',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).FREQ',
                    type: 'dimension',
                  },
                ],
                position: 2,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).FREQ',
                localRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_FREQ(1.0)' },
              },
              {
                id: 'SURVEY',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).SURVEY',
                    type: 'dimension',
                  },
                ],
                position: 3,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).SURVEY',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_SURVEY(1.0)',
                },
              },
              {
                id: 'REPRESENTED_VARIABLE',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).REPRESENTED_VARIABLE',
                    type: 'dimension',
                  },
                ],
                position: 4,
                type: 'Dimension',
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).REPRESENTED_VARIABLE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_REPRESENTED_VARIABLE(1.0)',
                },
              },
              {
                id: 'OCU',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).OCU',
                    type: 'dimension',
                  },
                ],
                position: 5,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_CLASSIF_TYPE(1.0).OCU',
                localRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ILO:CL_OCU(1.0)' },
              },
            ],
            timeDimensions: [
              {
                id: 'TIME_PERIOD',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.TimeDimension=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).TIME_PERIOD',
                    type: 'timedimension',
                  },
                ],
                position: 6,
                type: 'TimeDimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).TIME_PERIOD',
                localRepresentation: {
                  textFormat: { textType: 'ObservationalTimePeriod', isSequence: false, isMultiLingual: false },
                },
              },
            ],
          },
          measureList: {
            id: 'MeasureDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.MeasureDescriptor=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).MeasureDescriptor',
                type: 'measuredescriptor',
              },
            ],
            primaryMeasure: {
              id: 'OBS_VALUE',
              links: [
                {
                  rel: 'self',
                  urn:
                    'urn:sdmx:org.sdmx.infomodel.datastructure.PrimaryMeasure=ILO:ILMS_ALL_MFL_TEMP_OCU_NB(1.0).OBS_VALUE',
                  type: 'primarymeasure',
                },
              ],
              conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ILO:CS_ILOSTAT(1.0).OBS_VALUE',
              localRepresentation: { textFormat: { textType: 'Double', isSequence: false, isMultiLingual: false } },
            },
          },
        },
      },
    ],
  },
};
