import debug from '../../debug'

const NAME = 'healthcheck'
export const service = {
  async get() {
    const { mongo, solr, config, startTime, configProvider } = this.globals()

    const members = await configProvider.getTenants()
    const res = {
      gitHash: config.gitHash,
      startTime,
      totalMembers: Object.keys(members).length,
    }

    try {
      await mongo.ping()
      res.mongo = 'OK'
    } catch (err) {
      debug.error(err)
      throw new Error('mongodb is unreachable!')
    }

    try {
      const collections = await solr.status()
      res.solr = 'OK'
    } catch (err) {
      debug.error(err)
      res.solr = 'KO'
    }

    return res
  },
}

const init = evtx => {
  evtx.use(NAME, service)
}

export default init
