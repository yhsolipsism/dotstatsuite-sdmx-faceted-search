import got from 'got'
import { createContext } from 'jeto'
import ConfigProvider, { getStopwordsUri, getSynonymsUri } from '..'

const CONFIG_URL = 'http://FAKE_URI'
const MEMBER = { id: 1 }
const APP_ID = 2
const SYNONYMS = { "jobs": "employment" };
const STOPWORDS = [ "a", "an" ];

describe('server | configProvider', () => {
  it('should get member', async () => {
    jest.spyOn(got, 'get').mockImplementation(() => ({
      json: () => Promise.resolve({ [MEMBER.id]: MEMBER })
    }))

    const config = { configUrl: CONFIG_URL }
    const provider = ConfigProvider(createContext({ config }))

    const member = await provider.getTenant(MEMBER.id)
    expect(member).toEqual(MEMBER)
  })

  it('should generate the synonym uri', () => {
    const uri = getSynonymsUri(MEMBER.id, APP_ID, 'en')
    expect(uri).toEqual(`/${MEMBER.id}/${APP_ID}/synonyms/en.json`)
  })

  it('should get synonyms content via http', async () => {
    jest.spyOn(got, 'get').mockImplementation(() => ({
      json: () => Promise.resolve(SYNONYMS)
    }))

    const config = { appId: 'APP_ID', configUrl: CONFIG_URL }
    const provider = ConfigProvider(createContext({ config }))

    const synonyms = await provider.getSynomyms(MEMBER, 'en')
    expect(synonyms).toEqual(SYNONYMS)
  })

  it('should generate the stopword uri', () => {
    const uri = getStopwordsUri(MEMBER.id, APP_ID, 'en')
    expect(uri).toEqual(`/${MEMBER.id}/${APP_ID}/stopwords/en.json`)
  })

  it('should get stopwords content via http', async () => {
    jest.spyOn(got, 'get').mockImplementation(() => ({
      json: () => Promise.resolve(STOPWORDS)
    }))

    const config = { appId: 'APP_ID', configUrl: CONFIG_URL }
    const provider = ConfigProvider(createContext({ config }))

    const synonyms = await provider.getStopwords(MEMBER, 'en')
    expect(synonyms).toEqual(STOPWORDS)
  })
})
