import Models from '../models'

export default async ctx => {
  const models = await Models(ctx)
  return ctx({ models })
}
