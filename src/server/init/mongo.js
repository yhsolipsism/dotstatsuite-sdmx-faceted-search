import { MongoClient } from 'mongodb'
import debug from '../debug'

export default ctx => {
  const {
    config: {
      mongo: { dbName, url },
    },
  } = ctx()
  const mongoclient = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true })
  return new Promise((resolve, reject) => {
    mongoclient.connect(err => {
      if (err) return reject(err)
      debug.info(`connected to mongodb ${dbName} database`)
      resolve(
        ctx({
          mongo: {
            client: mongoclient,
            database: mongoclient.db(dbName),
            dropDatabase: () => mongoclient.db(dbName).dropDatabase(),
            ping: () => mongoclient.db(dbName).command({ ping: 1 }),
            close: () => mongoclient.close(),
          },
        }),
      )
    })
  })
}
