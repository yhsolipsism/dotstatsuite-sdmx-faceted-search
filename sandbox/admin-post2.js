import request from 'axios';
import util from 'util';

request({
  method: 'POST',
  headers: { 'x-api-key': 'secret' },
  url: 'http://localhost:7300/admin/dataflows?mode=stream&tenant=oecd',
  strictSSL: false,
  responseType: 'stream',
}).then(response => {
  const stream = response.data;
  stream.on('data', chunk => {
    console.log(util.inspect(JSON.parse(chunk.toString()), null, null));
  });
  // stream.on('end', () => { console.log('END'); });
});
