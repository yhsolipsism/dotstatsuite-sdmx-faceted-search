import axios from 'axios';
import { prop } from 'ramda';

const run = body => axios.post('http://localhost:3007/api/search', body).then(prop('data'));
run({
  search: 'datasourceId:SIS-CC-stable dataflowId:"DF_SDG_ALL_SDG_0852_SEX_DSB_RT"',
}).then(console.log);
