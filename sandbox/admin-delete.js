import axios from 'axios';
import { prop } from 'ramda';

let url = 'https://sfs-pre-prod-oecd.redpelicans.com';
url = 'http://localhost:7300';
const key = 'secret';

const run = () =>
  Promise.all([
    axios.delete(`${url}/admin/config`, { headers: { 'x-api-key': key } }).then(prop('data')),
    axios.delete(`${url}/admin/dataflows`, { headers: { 'x-api-key': key } }).then(prop('data')),
  ]);
run().then(console.log);
