docker volume rm solr-sdmx
docker run --rm -v solr-sdmx:/opt/solr/server/solr/mycores solr:7 bash -c "precreate-core sdmx-facet-search"
docker run -d --rm --name init-solr -v solr-sdmx:/root alpine tail -f /dev/null
# docker exec -it init-solr rm /root/dbnomics/conf/managed-schema
# docker cp $IMPORTER_PATH/solr_core_config/schema.xml init-solr:/root/dbnomics/conf
docker cp ./solr_core_config/solrconfig.xml init-solr:/root/sdmx-facet-search/conf
docker rm -f init-solr
docker run -p 8983:8983 -v solr-sdmx:/opt/solr/server/solr/mycores --restart=always --name=solr -d solr:7

