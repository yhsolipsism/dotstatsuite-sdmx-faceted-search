import faker from 'faker';
import R from 'ramda';
import fs from 'fs';

const random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const uniq = (faker, cache = {}) => {
  return () => {
    const getValue = () => {
      const value = faker().toUpperCase();
      return cache[value] ? getValue() : value;
    };
    const value = getValue();
    cache[value] = 1;
    return value;
  };
};

const loadCategories = (schema, path = '', values = [], level = 0) => {
  if (!schema) return [];
  return schema.reduce((res, [cat, children]) => {
    const newPath = `${path}/${cat}`;
    const newValue = `${level}${newPath}/`;
    const newValues = [...values, newValue];
    return [...res, newValues, ...loadCategories(children, newPath, newValues, level + 1)];
  }, []);
};

const locale = lang => faker => () => `${faker()} (${lang.toUpperCase()})`;
const localeEN = locale('en');
const localeFR = locale('fr');
const CATEGORIES = [
  [
    'Business & Industrial',
    [
      ['Electrical & test Equipment'],
      ['Healthcare, Lab & Life Science'],
      [
        'Automation, Motor & Drives',
        [
          [
            'Control Systems & PLCs',
            [
              ['PLC Processors'],
              ['PLC Peripherical Modules'],
              ['HMI & Open Interface Panels'],
              ['PLC Cables'],
              ['PLC Chassis'],
            ],
          ],
          ['Drives & Motion Control', [['Motor Drives & Controls'], ['Pneumatic Cylinders']]],
          ['Electric Motors', [['Servo Motors'], ['Geamotors']]],
        ],
      ],
    ],
  ],
  ['Music', [['Records'], ['Cassettes'], ['CDs']]],
  [
    'Motors',
    [
      ['Automotive Tools & Supplies'],
      ['Parts & Accessories', [['Car & Truck Parts'], ['Manuals & Literature'], ['Motorcycle Parts']]],
    ],
  ],
  ['Coins & Paper Money', [['Paper Money: World'], ['Publication & Supplies'], ['Exonumia', [['World'], ['Medals']]]]],
];

const allCategories = loadCategories(CATEGORIES);
console.log(allCategories);

const getDomainValues = domain => () => domain.filter((v, i) => random(0, 1));

const FREQUENCIES = [
  {
    fr: 'Jour (D)',
    en: 'Daily (D)',
  },
  {
    fr: 'Mois (M)',
    en: 'Monthly (M)',
  },
  {
    fr: 'Annee (A)',
    en: 'Yearly (Y)',
  },
];

const COUNTRIES = R.uniq(R.times(() => `${faker.address.country()} (${faker.address.countryCode()})`, 20));
const INTEREST_RATE_TYPES = [
  'Bank interest rates (B)',
  'Money market interest rates (M)',
  'Long-term interest rate for convergence purposes (L)',
];

const name = () => `${faker.hacker.adjective()} ${faker.commerce.productAdjective()} ${faker.commerce.productName()}`;
const COUNT = 1;

const schema = {
  id: uniq(() => `${faker.random.word()}-${random(0, 1000)}`),
  type_s: () => 'dataflow',
  agency_s: faker.hacker.abbreviation,
  version_s: () => faker.system.semver(),
  name_en_txt: localeEN(name),
  name_fr_txt: localeFR(name),
  description_en_txt: localeEN(faker.lorem.sentence),
  description_fr_txt: localeFR(faker.lorem.sentence),
  category_ss: () => allCategories[random(0, allCategories.length - 1)],
  _frequencies: getDomainValues(FREQUENCIES),
  frequency_fr_ss: R.compose(
    R.chain(R.prop('fr')),
    R.prop('_frequencies'),
  ),
  frequency_en_ss: R.compose(
    R.chain(R.prop('en')),
    R.prop('_frequencies'),
  ),
  reference_area_ss: getDomainValues(COUNTRIES),
  interest_rate_type_ss: getDomainValues(INTEREST_RATE_TYPES),
};

const genDataflow = R.compose(
  R.reduce((obj, [prop, method]) => ({ ...obj, [prop]: method(obj, prop) }), {}),
  R.toPairs,
);
const getPrivateProps = R.compose(
  R.filter(x => x[0] === '_'),
  R.keys,
);
const privateProps = getPrivateProps(schema);
const filterPrivateProps = R.omit(privateProps);
const genDataflows = R.compose(
  filterPrivateProps,
  genDataflow,
);

const res = R.times(() => genDataflows(schema), COUNT);

fs.writeFile('/tmp/data2.js', JSON.stringify(res), err => {
  if (err) return console.log(err);
  console.log('/tmp/data2.js saved!');
});
